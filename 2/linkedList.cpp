#include <iostream>
#include "linkedList.h"

/* adds a new node to the start of a linked list
input: pointer to start of linked list, value to enter
output: none
*/
void addValue(node** head, int value)
{
	node* newNode = new node;
	node* temp = *head;
	newNode->_value = value;
	if (*head)
	{
		newNode->_next = temp;
	}
	else
	{
		newNode->_next = NULL;
	}
	*head = newNode;
}

/* removes the first node in a given linked list
input: pointer to first node
output: none
*/
void remove(node** head)
{
	node* temp = *head;
	if (*head)
	{
		*head = temp->_next;
		delete (temp);
	}
}



