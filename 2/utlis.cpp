#include <iostream>
#include "utlis.h"
#include "linkedList.h"
#include "stack.h"

/* reverse a given array 
input: array, size of the array
output: none
*/
void reverse(int* nums, unsigned int size)
{
	int i = 0;
	stack s = { NULL, 0 };
	for (i = 0; i < size; ++i)
	{
		push(&s, *(nums + i));
	}
	for (i = 0; i < size; ++i)
	{
		*(nums + i) = pop(&s);
	}
	cleanStack(&s);
}

/* gets from the user ten positive whole numbers and puts them in a reverse order in an array
input: none
output: the array with the reversed order
*/
int* reverse10()
{
	int* arr = new int[SIZE];
	int i = 0;
	for (i = 0; i < SIZE; ++i)
	{
		std::cout << "Please enter value for arr[" << i << "]: ";
		std::cin >> *(arr + i);
	}
	reverse(arr, SIZE);
	return arr;
}