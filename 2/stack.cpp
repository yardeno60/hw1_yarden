#include "stack.h"

/* adds an item to the stack
input: pointer to stack, item to add
output: none
*/
void push(stack* s, unsigned int element)
{
	node** head = &s->_elements;
	addValue(head, element);
	++s->_count;
}

/* removes the last item added to the stack
input: pointer to stack
output: the removed item
*/
int pop(stack* s)
{
	int value = -1;
	node** head = &s->_elements;
	if (s->_count != 0)
	{
		value = s->_elements->_value;
		remove(head);
		--s->_count;
	}
	return value;
}

/* creats a new stack with starter values
input: pointer to stack (NULL at the beginning)
output: none
*/
void initStack(stack* s)
{
	s->_count = 0;
	s->_elements = new node;
}

/* removes the linked list inside the stack from the memory 
input: pointer to stack\
output: none
*/
void cleanStack(stack* s)
{
	node* curr = s->_elements;
	node* temp = NULL;
	while (curr)
	{
		temp = curr->_next;
		delete curr;
		curr = temp;
	}
}
