#ifndef LINKED_H
#define LINKED_H

typedef struct node
{
	unsigned int _value;
	node* _next;
} node;

void addValue(node** head, int value);
void remove(node** head);
void testPrint(node* head);

#endif /* LINKED_H */