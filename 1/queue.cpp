#include <iostream>
#include "queue.h"

/* crates a new queue
input: pointer to queue, size of queue
output: none
*/
void initQueue(queue* q, unsigned int size)
{
	unsigned int i = 0;
	q->_elements = new int[size];
	q->_maxSize = size;
	q->_count = 0;
	for (i = 0; i < size; ++i)
	{
		*(q->_elements + i) = -1;
	}
}

/* deletes the queue from the memory
input: pointer to queue
output: none
*/
void cleanQueue(queue* q)
{
	delete[] q->_elements;
	delete q;
}

/* adds a new int to the queue
input: pointer to queue, int to add
output: none
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q->_count != q->_maxSize)
	{
		*(q->_elements + q->_count) = newValue;
		++q->_count;
	}
}

/* removes the first item in queue
input: pointer to queue
output: first item in queue
*/
int dequeue(queue* q)
{
	int pop = *(q->_elements);
	int i = 0;
	for (i = 0; i < q->_count; ++i)
	{
		*(q->_elements + i) = *(q->_elements + i + 1);
	}
	if (q->_count != 0)
	{
		*(q->_elements + q->_count - 1) = -1;
		--q->_count;
	}

	return pop;
}

